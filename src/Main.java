import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
public class Main {
    static Connection conn;
    public static void main(String[] arts) throws SQL{
        conn =
                DriverManager.getConnection("jdbc:mysql://localhost:3306/student_info");
        LocalDate N = LocalDate.of(2023, 01,18);
        String SQL_QUERY =String.format (
                """
                SELECT last_name, first_name, sur_name
                FROM student
                JOIN grade_book ON student.id = grade_book.student_id
                WHERE grade_book.exam_date < "%s";
                """, N.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        PreparedStatement studentsQuerry = conn.prepareStatement(SQL_QUERY);

        ResultSet studentsSet = studentsQuerry.executeQuery();

        System.out.println("Фамилия " + "-".repeat(10) + " Имя " + "-".repeat(10) + " Отчество");
        while (studentsSet.next()) {
            System.out.println(studentsSet.getString(1) + " " + studentsSet.getString(2), studentsSet.getString(3));
        }
    }
}

